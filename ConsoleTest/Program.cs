﻿using System;
using JsonCommand;

namespace ConsoleTest
{
    internal class Program
    {
        private static void Main()
        {
            //Init context and command engine
            var context = new AppContext {ContextName = "MyApp context"};
            JsonCommandEngine.ErrorHandler = ErrorHandler;
            JsonCommandEngine.Context = context;

            //test command
            var command = new HellowWorldRequest {SenderName = "Vasya"};
            var json = command.ToJson();

            //Recive message
            //-----------------------------------------
            JsonCommandEngine.EnqueueMessage(json);

            //This method I call in infinity loop with Thread.Sleep(1) 
            //best perfomance with Thread Count = Environment.ProcessorCount
            JsonCommandEngine.ProcessMessage();

            //-----------------------------------------
            Console.ReadKey();
        }

        private static void ErrorHandler(Exception exception)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(exception);
            Console.ResetColor();
        }
    }

    public class AppContext
    {
        public int ProcessorsCount
        {
            get { return Environment.ProcessorCount; }
        }

        public string ContextName { get; set; }
    }

    public class HellowWorldRequest : BaseRequest
    {
        public string SenderName { get; set; }
    }

    public class HellowWorldHandler : BaseHandler<HellowWorldRequest, AppContext>
    {
        public override void Handle(HellowWorldRequest command, AppContext context)
        {
            Console.WriteLine("HellowWorld from {0}, in context '{1}'", command.SenderName, context.ContextName);
        }
    }
}