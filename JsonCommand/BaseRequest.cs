﻿namespace JsonCommand
{
    public abstract class BaseRequest : BaseCommand
    {
        protected BaseRequest()
        {
            Type = CommandType.Request;
        }
    }
}