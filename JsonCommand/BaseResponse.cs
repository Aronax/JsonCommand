namespace JsonCommand
{
    public abstract class BaseResponse : BaseCommand
    {
        protected BaseResponse()
        {
            Type = CommandType.Response;
        }
    }
}