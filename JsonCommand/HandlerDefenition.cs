﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace JsonCommand
{
    public class HandlerDefenition
    {
        public HandlerDefenition(Type handlerType, Type commandType, object handlerInstance, MethodInfo hadlerMethod, InstanceMode instanceMode)
        {
            HandlerType = handlerType;
            CommandType = commandType;
            HandlerInstance = handlerInstance;
            HadlerMethod = hadlerMethod;
            InstanceMode = instanceMode;
        }

        public Type HandlerType { get; set; }
        public Type CommandType { get; set; }
        public object HandlerInstance { get; set; }
        public MethodInfo HadlerMethod { get; set; }
        public InstanceMode InstanceMode { get; set; }

        public static IEnumerable<Type> GetAllImplementations<T>()
        {
            var typeList = new List<Type>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                typeList.AddRange(assembly.GetTypes().Where(IsImplementation<T>));
            }
            return typeList;
        }

        private static bool IsImplementation<T>(Type type)
        {
            var baseType = typeof(T);
            return type.GetInterface(baseType.Name) != null && type.IsAbstract == false;
        }

        public static List<Type> GetAllInheritance<T>() where T : class
        {
            var typeList = new List<Type>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                typeList.AddRange(assembly.GetTypes()
                                          .Where(IsInheritance<T>));
            }
            return typeList;
        }

        private static bool IsInheritance<T>(Type type)
        {
            var baseType = typeof(T);
            return type.IsClass && !type.IsAbstract && type.IsSubclassOf(baseType);
        }

        public static ConcurrentDictionary<Type, List<HandlerDefenition>> GetMessageHandlerDefenitions()
        {
            var result = new ConcurrentDictionary<Type, List<HandlerDefenition>>();
            var types = GetAllImplementations<IBaseHandler>();

            foreach (var handlerType in types)
            {
                var mode = InstanceMode.Single;
                var attrib = handlerType.GetCustomAttribute<InstanceModeAttribute>();

                if (attrib != null)
                    mode = attrib.InstanceMode;

                var baseType = handlerType.BaseType;
                if (baseType == null || !baseType.IsGenericType)
                    continue;

                var dataType = baseType.GetGenericArguments().FirstOrDefault();
                if (dataType == null)
                    continue;

                if (!result.ContainsKey(dataType))
                    result[dataType] = new List<HandlerDefenition>();

                result[dataType]
                    .Add(new HandlerDefenition(handlerType,
                                               dataType,
                                               mode == InstanceMode.Single
                                                   ? Activator.CreateInstance(handlerType)
                                                   : null,
                                               handlerType.GetMethod("Handle"),
                                               mode));
            }
            return result;
        }
    }
}