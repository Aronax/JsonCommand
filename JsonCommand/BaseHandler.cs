namespace JsonCommand
{
    public abstract class BaseHandler<T, TContext> : IBaseHandler where T : BaseCommand
    {
        public abstract void Handle(T command, TContext context);
    }
}