﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace JsonCommand
{
    public static class JsonCommandEngine
    {
        private static readonly ConcurrentDictionary<Type, List<HandlerDefenition>> _handlerDefenitions = new ConcurrentDictionary<Type, List<HandlerDefenition>>();
        private static readonly ConcurrentQueue<string> _inMessages = new ConcurrentQueue<string>();
        public static Action<Exception> ErrorHandler { get; set; }
        public static Object Context { get; set; }


        static JsonCommandEngine()
        {
            _handlerDefenitions = HandlerDefenition.GetMessageHandlerDefenitions();
        }

        public static void EnqueueMessage(string message)
        {
            _inMessages.Enqueue(message);
        }

        public static void ProcessMessage()
        {
            string message;
            while (_inMessages.TryDequeue(out message))
            {
                try
                {
                    HandleMessage(message);
                }
                catch (Exception ex)
                {
                    if (ErrorHandler != null)
                        ErrorHandler(ex);
                }
            }
        }

        private static void HandleMessage(string message)
        {
            var command = BaseCommand.FromJson(message);
            if (command == null)
                return;

            List<HandlerDefenition> items;
            if (!_handlerDefenitions.TryGetValue(command.GetType(), out items))
                return;

            foreach (var item in items)
            {
                InvokeHandler(item, command);
            }
        }

        private static void InvokeHandler(HandlerDefenition handler, BaseCommand command)
        {
            try
            {
                var instance = handler.InstanceMode == InstanceMode.Single
                                   ? handler.HandlerInstance
                                   : Activator.CreateInstance(handler.HandlerType);

                handler.HadlerMethod.Invoke(instance, new[] {command, Context});
            }
            catch (Exception ex)
            {
                if (ErrorHandler != null)
                    ErrorHandler(ex);
            }
        }
    }
}