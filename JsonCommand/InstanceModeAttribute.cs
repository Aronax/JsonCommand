﻿using System;

namespace JsonCommand
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InstanceModeAttribute : Attribute
    {
        public InstanceModeAttribute(InstanceMode mode = InstanceMode.Single)
        {
            InstanceMode = mode;
        }

        public InstanceMode InstanceMode { get; private set; }
    }
}