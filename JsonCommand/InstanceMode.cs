﻿namespace JsonCommand
{
    public enum InstanceMode
    {
        Single = 0,
        PerCall = 1
    }
}